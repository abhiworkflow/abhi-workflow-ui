export class CreateBusinessModel {
    general_information: GeneralInformationModel;
    policy_holder: PolicyHolderModel;
    gst_payment: GstPaymentModel;
    abhi_intermediary: AbhiIntermediaryModel;
    tpa_business_segment: TpaBusinessSegmentModel;
    policy: PolicyModel;
    policy_servicing: PolicyServicingModel;
}

export class GeneralInformationModel {
    broker_id: number;
    sales_code: string;
    select_files_to_upload: any[];
    note: string;
    imd_code: string;
    imd_code_note_created: boolean;
}

export class PolicyHolderModel {
    name_of_the_policyholder: string;
    complete_address_of_client: string;
    finalized_quote_date_mail_date: string;
    product: string;
    finalized_quote_number_mail: string;
    name_of_the_contact_person: string;
    mobile_number: string;
    email_id_for_correspondence: string;
}

export class GstPaymentModel {
    gst_registration_status: string;
    gst_state: string;
    gst_pan_number: string;
    series_number_gstn: string;
    amount_paid_by: string;
    receipt_amount: string;
    premium: string;
    cash_deposit: string;
}

export class AbhiIntermediaryModel {
    name_of_the_sales_manager: string;
    mobile_number: string;
    email_id_for_correspondence: string;
    imd_contact_person: string;
    imd_email_id_for_correspondence: string;
    imd_mobile_number: string;
    branch_reference_id: string;
    bank_branch_name: string;
    partner_reference_id: string;
    partner_sales_employee_id: string;
    location: string;
}

export class TpaBusinessSegmentModel {
    name_of_the_tpa: string;
    mobile_number: string;
    name_of_previos_tpa: string;
    email_id_for_correspondence: string;
    location: string;
    sme: string;
    mid_and_large: string;
    affinity: string;
    creditor: string;
    rural: string;
    abg: string;
    top_up: string;
}

export class PolicyModel {
    policy_period_start_date: string;
    policy_period_end_date: string;
    policy_category: string;
    payment_type: string;
    if_instalmemt_frequency: string;
    co_insurance: string;
}

export class PolicyServicingModel {
    policy_document_to_be_sent_to: string;
    complete_address_for_policy_dispatch: string;
    health_card_issuance: string;
    regular_communications_to_be_sent_to: string;
    endorsements_method: string;
    claim_payment_to: string;
    finalised_quote_no_option_no: string;
}