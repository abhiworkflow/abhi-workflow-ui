export class CreateClientModel {
    client_information_1: ClientInformation1Model;
    client_information_2: ClientInformation2Model;
    client_information_3: ClientInformation3Model;
}

export class ClientInformation1Model {
    customer_type: string;
    govt_undertaking: string;
    eta_account_available: string;
    apply_for_eta_account: string;
    salutation: string;
    pincode: string;
    name_of_company: string;
    company_type: string;
    date_of_incorporation: string;
    industry_type: string;
    nationality: string;
    service_tax_registration_no: string;
    tan_no: string;
    company_registration_no: string;
    is_abg_policy: string;
    do_you_have_pan: string;
    pan_number: string;
}

export class ClientInformation2Model {
    primary_email_id: string;
    contact_person: string;
    client_mobile_number: string;
    std_landline_no: string;
    start_date: string;
    end_date: string;
    others: string;
    remarks: string;
    status: string;
    gst_registration_status: string;
    sun_sidary: string;
    state_gst: string;
    pan_number_gst: string;
    series_number_gst: string;
    go_green: string;
    note: string;
}

export class ClientInformation3Model {
    is_parent_child_available: string;
    select_tagging: string;
    tag_parent_child_id: string;
}