export class CreateQuoteModel {
    general_proposal: GeneralProposalModel;
    coinsurance: CoinsuranceModel;
    risk_details: RiskDetailsModel;
    past_policy: PastPolicyModel;
    premium: PremiumModel;
}

export class GeneralProposalModel {
    client_information:ClientInformationModel;
    business_source_information:BusinessInformationModel;
    proposal_information: ProposalInformationModel;
}

export class ClientInformationModel {
    client_code_name: string;
    gstin: string;
    dispatch_mode: string;
    email: string;
    previous_cancelled: string;
    reason: string;
    process_remarks: string;
    business_type: string;
    transaction_type: string;
    product_name_code: string;
    plan_scheme: boolean;
    policy_variant: string;
    document_control_no: string;
    cover_type: string;
}

export class BusinessInformationModel {
    office_code_location: string;
    business_source_name: string;
    sales_manager_name_code: string;
    intermediary_channel_name_code: string;
    business_channel_type: string;
    business_servicing_channel_type: string;
    business_channel_branch: string;
    business_servicing_channel: string;
    brokerage_commision_payable: string;
    share: string;
    is_multiple_intermediary: boolean;
    branch_reference_id: string;
    sector: string;
    bank_branch_name: string;
}

export class ProposalInformationModel {
    proposal_received_date_by_ops: string;
    proposal_received_time_by_ops: string;
    proposal_issue_date: string;
    proposal_issue_Time: string;
    policy_start_date: string;
    policy_start_time: string;
    policy_end_date: string;
    policy_end_time: string;
    policy_tenure: string;
    is_proposal_issued: string;
    proposal_number: string;
    place_of_issue: string;
    proposal_amount: string;
    rural_urban: string;
    option_for_calculation: string;
    is_it_master_policy: string;
    ri_inward: string;
    is_it_pre_underwritten: string;
    service_tax_exemption_category: string;
    is_premium_config: string;
    coinsurance_status: string;
    is_banca_policy: string;
    own_share: string;
    sme_code: string;
    remarks: string;
    condition_clauses: string;
}

export class CoinsuranceModel {
    co_insurance_type: string;
    unique_reference_code: string;
    full_commission_leader: boolean;
    s_tax_leader: boolean;
    policy_number: string;
    co_insurer_type: string;
    insurance_company: string;
    branch: string;
    office_code: string;
    address: string;
    share: string;
    administrative_charges: string;
    administrative_charges_rate: string;
    penal_charges: string;
    penal_charges_rate: string;
    other_remarks: string;
}

export class RiskDetailsModel {
    is_member_data_available: string;
    proposal_basis: string;
    industry_type: string;
    insured_person_relationship: string;
    description: string;
    tax_type: string;
    corporate_floater: string;
    family_unit_definition: string;
    premium_calculation_basis: string;
    credit_limit: string;
    free_look_period: string;
}

export class PastPolicyModel {

}

export class PremiumModel {

}