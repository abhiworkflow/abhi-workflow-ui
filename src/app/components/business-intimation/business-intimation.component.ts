import { Component, OnInit } from '@angular/core';
import { CreateBusinessComponent } from 'src/app/components/business-intimation/create-business/create-business.component';
import { ViewChild } from '@angular/core';
import { RouteUrls } from 'src/app/misc/constants';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { PopupComponent } from 'src/app/components/popup/popup.component';

@Component({
  selector: 'app-business-intimation',
  templateUrl: './business-intimation.component.html',
  styleUrls: ['./business-intimation.component.scss']
})
export class BusinessIntimationComponent implements OnInit {

  homeRfqLink =  `../${RouteUrls.home}`;
  
  tab1Lable = "Create Business Information";
  tab2Lable = "Download";

  count = 1;

  @ViewChild('createBusiness')
  createBusiness: CreateBusinessComponent;

  @ViewChild('successModal')
  successModal: PopupComponent;

  clientId: string = null;
  clientDetails: any = null;
  
  constructor(private router: Router, private route: ActivatedRoute) {
    this.route.paramMap.subscribe(params => {
      if (params.get('status')) {
        let df = params.get('status');
        console.log(df);
        this.getClientDetails();
      }
    });
  }

  ngOnInit() {
  }

  backNext() {
    if(this.count > 1) {
      this.count -= 1;
    }
  }
  
  buttonNext() {
    let data = this.createBusiness.getData();
    console.log(data);
    if(data) {
      
      if(this.count < 7) {
        this.count += 1;
      } else {
        console.log("goto client Page");
        this.clientId = "3232";
        this.showSuccessModal();
      }
      console.log(this.count);
    }
    
  }

  getClientDetails() {
    // Server call
    this.clientDetails = {
      'name' : "sagar",
      'files' : ['ds1', 'ds2'],
      'note' : 'dsdsnote'
    }
  }

  downloadFile(filename: string) {
    console.log(filename);
  }

  backTab2() {
    this.router.navigate([RouteUrls.notifications, {status : 'Download'}]);
  }

  generateSearchClientId() {
    this.router.navigate([RouteUrls.clients]);
  }

  showSuccessModal() {
    this.successModal.showSuccessModal();
  }

  hideSuccessModal() {
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }
}
