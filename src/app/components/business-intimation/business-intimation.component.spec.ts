import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessIntimationComponent } from './business-intimation.component';

describe('BusinessIntimationComponent', () => {
  let component: BusinessIntimationComponent;
  let fixture: ComponentFixture<BusinessIntimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessIntimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessIntimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
