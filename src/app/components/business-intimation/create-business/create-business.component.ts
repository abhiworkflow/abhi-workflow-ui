import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { UploadFilesComponent } from 'src/app/components/upload-files/upload-files.component';

@Component({
  selector: 'app-create-business',
  templateUrl: './create-business.component.html',
  styleUrls: ['./create-business.component.scss']
})
export class CreateBusinessComponent implements OnInit {

  @Input()
  formNumber: number;

  // File Upload
  @ViewChild('uploadFileModal')
  uploadFileModal: UploadFilesComponent;

  generalInformation: FormGroup;
  policyHolderDetails: FormGroup;
  gstPaymentDetails: FormGroup;
  abhiIntermediaryDetails: FormGroup;
  tpaBusinessSegmentDetails: FormGroup;
  policyDetails: FormGroup;
  policyServicing: FormGroup;

  constructor(public fb: FormBuilder) {
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    this.generalInformation = this.fb.group({
      broker_id: ['', Validators.required],
      sales_code: ['', Validators.required],
      select_files_to_upload: this.fb.array([]),
      note: [''],
      imd_code: ['', Validators.required],
      imd_code_note_created: [''],
    });

    this.policyHolderDetails = this.fb.group({
      name_of_the_policyholder: ['', Validators.required],
      complete_address_of_client: ['', Validators.required],
      finalized_quote_date_mail_date: ['', Validators.required],
      product: ['', Validators.required],
      finalized_quote_number_mail: [''],
      name_of_the_contact_person: ['', Validators.required],
      mobile_number: ['', Validators.required],
      email_id_for_correspondence: ['', Validators.compose([Validators.required, Validators.email])]
    });

    this.gstPaymentDetails = this.fb.group({
      gst_registration_status: ['', Validators.required],
      gst_state: ['', Validators.required],
      gst_pan_number: ['', Validators.required],
      series_number_gstn: ['', Validators.required],
      amount_paid_by: ['', Validators.required],
      receipt_amount: ['', Validators.required],
      premium: [''],
      cash_deposit: ['']
    });

    this.abhiIntermediaryDetails = this.fb.group({
      name_of_the_sales_manager: ['', Validators.required],
      mobile_number: ['', Validators.required],
      email_id_for_correspondence: ['', Validators.compose([Validators.required, Validators.email])],
      imd_contact_person: ['', Validators.required],
      imd_email_id_for_correspondence: ['', Validators.compose([Validators.required, Validators.email])],
      imd_mobile_number: ['', Validators.required],
      branch_reference_id: [''],
      bank_branch_name: [''],
      partner_reference_id: [''],
      partner_sales_employee_id: [''],
      location: [''],
    });

    this.tpaBusinessSegmentDetails = this.fb.group({
      name_of_the_tpa: [''],
      mobile_number: [''],
      name_of_previos_tpa: [''],
      email_id_for_correspondence: ['', Validators.compose([Validators.required, Validators.email])],
      location: [''],
      sme: [''],
      mid_and_large: [''],
      affinity: [''],
      creditor: [''],
      rural: [''],
      abg: [''],
      top_up: [''],
    });

    this.policyDetails = this.fb.group({
      policy_period_start_date: ['', Validators.required],
      policy_period_end_date: ['', Validators.required],
      policy_category: ['', Validators.required],
      payment_type: ['', Validators.required],
      if_instalmemt_frequency: ['', Validators.required],
      co_insurance: ['', Validators.required]
    });

    this.policyServicing = this.fb.group({
      policy_document_to_be_sent_to: ['', Validators.required],
      complete_address_for_policy_dispatch: ['', Validators.required],
      health_card_issuance: ['', Validators.required],
      regular_communications_to_be_sent_to: ['', Validators.required],
      endorsements_method: ['', Validators.required],
      claim_payment_to: ['', Validators.required],
      finalised_quote_no_option_no: ['']
    });
  }

  getFormControl(controlName: string): AbstractControl {
    switch (this.formNumber) {
      case 1:
        return this.generalInformation.get(controlName);
        break;
      case 2:
        return this.policyHolderDetails.get(controlName);
        break;
      case 3:
        return this.gstPaymentDetails.get(controlName);
        break;
      case 4:
      return this.abhiIntermediaryDetails.get(controlName);
        break;
      case 5:
      return this.tpaBusinessSegmentDetails.get(controlName);
        break;
      case 6:
      return this.policyDetails.get(controlName);
        break;
      case 7:
        return this.policyServicing.get(controlName);
        break;
    }
  }

  fileUpload() {
    console.log("fileUpload");
    this.uploadFileModal.showModal();
    this.uploadFileModal.showModal().then(v => {
     
      if (v == null) {
        return "";
      }
      
      if(v.length > 0) {
        v.forEach(element => {
          //this.uploadFile(element);
        });
      } else {
        //this.uploadFile(v);
      }

      console.log(v);
      console.log("server call");
    }).catch(err => { });
  }

  getData() {
    let objectValue = null;

    switch (this.formNumber) {
      case 1:
        if (this.generalInformation.valid) {
          objectValue = { "general_information" : this.generalInformation.value };
        } else {
          Object.keys(this.generalInformation.controls).forEach(field => {
            const control = this.generalInformation.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 2:
        if (this.policyHolderDetails.valid) {
          objectValue = { "policy_holder_details" : this.policyHolderDetails.value };
        } else {
          Object.keys(this.policyHolderDetails.controls).forEach(field => {
            const control = this.policyHolderDetails.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 3:
        if (this.gstPaymentDetails.valid) {
          objectValue = { "gst_payment_details" :this.gstPaymentDetails.value };
        } else {
          Object.keys(this.gstPaymentDetails.controls).forEach(field => {
            const control = this.gstPaymentDetails.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 4:
        if (this.abhiIntermediaryDetails.valid) {
          objectValue = { "abhi_intermediary_details" :this.abhiIntermediaryDetails.value };
        } else {
          Object.keys(this.abhiIntermediaryDetails.controls).forEach(field => {
            const control = this.abhiIntermediaryDetails.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 5:
        if (this.tpaBusinessSegmentDetails.valid) {
          objectValue = { "tpa_business_segment_details" :this.tpaBusinessSegmentDetails.value};
        } else {
          Object.keys(this.tpaBusinessSegmentDetails.controls).forEach(field => {
            const control = this.tpaBusinessSegmentDetails.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 6:
        if (this.policyDetails.valid) {
          objectValue = { "policy_details" :this.policyDetails.value};
        } else {
          Object.keys(this.policyDetails.controls).forEach(field => {
            const control = this.policyDetails.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 7:
        if (this.policyServicing.valid) {
          objectValue = { "policy_servicing" :this.policyServicing.value};
        } else {
          Object.keys(this.policyServicing.controls).forEach(field => {
            const control = this.policyServicing.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
    }

    return objectValue;
  }
}
