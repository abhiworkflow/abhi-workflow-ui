import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RouteUrls } from 'src/app/misc/constants';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  showError = false;
  error = "";
  loginForm: FormGroup;
  redirectUrl: string;
  username: string;
  password: string;
  constructor(fb: FormBuilder, private authService: AuthService, private router: Router, private route: ActivatedRoute) {

    this.loginForm = fb.group({
      username: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    });

    this.route.queryParams.subscribe(params => {
      this.redirectUrl = params["redirectUrl"];
    });

  }

  ngOnInit() {
  }

  signin() {
    this.authService.signIn(this.loginForm.value.username, this.loginForm.value.password)
      .then(result => {
        if (result && result.IsValidUser) {
          this.router.navigate([this.redirectUrl || `/${RouteUrls.home}`]);
        }
        else {
          this.showError = true;
          this.error = "Invalid credentials...";
        }
      }).catch(e => {
        console.log(e);
      })
  }

}
