import { Component, OnInit, ViewChild } from '@angular/core';
import { RouteUrls } from 'src/app/misc/constants';
import { ModalDirective } from 'ngx-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  businessIntimationLink = `../${RouteUrls.businessIntimation}`;

  @ViewChild('actionRequiredModal')
  actionRequiredModal: ModalDirective;

  private gridApi;
  private gridColumnApi;

  public rowSelection = "single";

  columnDefs = [
    {headerName: 'Client Name', field: 'client_name' },
    {headerName: 'User', field: 'user' },
    {headerName: 'Action Taken', field: 'action_taken'},
    {headerName: 'Pending Action', field: 'pending_action'},
  ];

  rowData = [
    { client_name: 'Toyota', user: 'Celica', action_taken: 35000, pending_action: "ss" },
    { client_name: 'Ford', user: 'Mondeo', action_taken: 32000, pending_action: "ss"},
    { client_name: 'Porsche', user: 'Boxter', action_taken: 72000, pending_action: "ss" }
  ];

  noteModel: string = null;

  selectedNotification: string = null;

  constructor(private router: Router, private route: ActivatedRoute) { 
    this.route.paramMap.subscribe(params => {
      if (params.get('status')) {
        this.selectedNotification = params.get('status');
        console.log(this.selectedNotification);
      }
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if(this.selectedNotification){
      this.showActionRequiredModal();
    }
  }

  textSearch(event) {
    const text = event.target.value;
    console.log(text);
  }

  onSelectionChanged() {
    var selectedRows = this.gridApi.getSelectedRows();
    console.log("onSelectionChanged");
    console.log(selectedRows);
    this.showActionRequiredModal();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  showActionRequiredModal() {
    this.actionRequiredModal.show();
  }

  hideActionRequiredModal() {
    this.noteModel = null;
    this.actionRequiredModal.hide();
  }

  takeAction() {
    console.log(this.noteModel);
    this.router.navigate([RouteUrls.businessIntimation, {status : 'Download'}]);
  }

  goToClientPage() {
    this.router.navigate([RouteUrls.clientDetails, '2222']);
  }

}
