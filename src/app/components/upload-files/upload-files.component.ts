import { Component, OnInit, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UploadEvent, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { Deferred } from 'src/app/Deferred';

@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss']
})
export class UploadFilesComponent implements OnInit {

  @ViewChild('uploadModal')
  uploadModal: ModalDirective;

  @Input()
  public AllowMultiple: boolean = false;

  private deferred: Deferred<any>;

  file: File = null;
  files: File[] = [];

  constructor(private ref: ChangeDetectorRef) { }

  ngOnInit() {
  }

  showModal(): Promise<any> {
    this.files = [];
    this.file = null;
    this.deferred = new Deferred<any>();
    this.uploadModal.show();

    return this.deferred.promise;
  }

  hideModal() {
    this.uploadModal.hide();
  }

  public notifyImageChanged() {
    this.deferred.resolve(this.AllowMultiple ? this.files : this.file);
    this.hideModal();
  }

  handleFileInput(fileInput: any) {
    this.files = [];

    if (this.AllowMultiple) {
      try {
        const files = fileInput.target.files;
        for (var i = 0; i < files.length; i++) {
          this.files.push(files[i]);
        }
      } catch (error) { }
    }
    else {
      this.file = fileInput.target.files[0];
    }

    fileInput.target.value = "";
  }

  public dropped(event: UploadEvent) {

    for (const droppedFile of event.files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          if (this.AllowMultiple)
            this.files.push(file);
          else
            this.file = file;
          this.ref.detectChanges();
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  public fileOver(event) {

  }

  public fileLeave(event) {

  }

}
