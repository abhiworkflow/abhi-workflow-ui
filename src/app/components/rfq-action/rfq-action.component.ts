import { Component, OnInit, ViewChild } from '@angular/core';
import { RfqService } from 'src/app/services/rfq.service';
import { PermissionService } from 'src/app/services/permission.service';
import { AgGridNg2 } from 'ag-grid-angular';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { RouteUrls } from 'src/app/misc/constants';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rfq-action',
  templateUrl: './rfq-action.component.html',
  styleUrls: ['./rfq-action.component.scss']
})
export class RfqActionComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;

  homeRfqLink = `../${RouteUrls.home}`;

  isEditing = false;
  user = "";
  selectedData: any = null;
  dataForm: FormGroup;
  columnDefs = [
    { headerName: 'CompanyName', field: 'Data.CompanyName',resizable: true    },
    { headerName: 'CompanyAddress', field: 'Data.CompanyAddress' ,resizable: true },
    { headerName: 'EmpCount', field: 'Data.EmpCount' ,resizable: true },
    { headerName: 'Amount', field: 'Data.Amount' ,resizable: true },
    { headerName: 'Status', field: 'Status' ,resizable: true },
    { headerName: 'Comment', field: 'Data.comment' ,resizable: true }
  ];

  comment: FormControl = new FormControl('');
  rowData = [];
  private gridApi;
  constructor(private router: Router,private rfqService: RfqService, private permission: PermissionService, fb: FormBuilder, private authService : AuthService) {

    this.dataForm = fb.group({
      CompanyName: [''],
      CompanyAddress: [''],
      EmpCount: [''],
      Amount: ['']
    });

  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit() 
  }

  ngOnInit() {
    this.user = this.authService.user;
    this.fetchRfq();
  }

  fetchRfq() {
    this.rfqService.getAllRfqs().then(v => {
      this.rowData = v.map(v => {
        return { ...v, Data: JSON.parse(v.Data) }
      }
      )
      console.log(this.rowData);
    });
  }

  signout(){
    this.authService.signOut();
    this.router.navigate([RouteUrls.signin]);
  }

  onRowSelected(data) {
    console.log(data);
    this.selectedData = data;
    const { CompanyName, CompanyAddress, EmpCount, Amount } = data.Data;
    this.dataForm.setValue({ CompanyName, CompanyAddress, EmpCount, Amount });
  }

  onSelectionChanged(event) {
    const selectedRows = event.api.getSelectedNodes();
    if (selectedRows.length == 0) {
      this.selectedData = null;
    } else {
      this.onRowSelected(selectedRows[0].data);
    }

    this.isEditing = false;
    this.dataForm.disable();
  }

  Approve() {
    const data = { Id: this.selectedData.Id, Data: { ...this.dataForm.value, comment: this.comment.value } };

  
    this.rfqService.approve(data).then(v => {
      if (v.Status && v.Status == "Success") {
        this.isEditing = false;
        this.selectedData = null;
        this.dataForm.disable();
        this.fetchRfq();
      }
    })


  }

  Reject() {
    const data = { Id: this.selectedData.Id, Data: { ...this.dataForm.value, comment: this.comment.value } };
    this.rfqService.reject(data).then(v => {
      if (v.Status && v.Status == "Success") {
        this.isEditing = false;
        this.selectedData = null;
        this.dataForm.disable();
        this.fetchRfq();      
      }
    })

  }

  Modify() {
    this.isEditing = true;
    this.dataForm.enable();
  }

  Save() {
    const data = { Id: this.selectedData.Id, Data: { ...this.dataForm.value, comment: this.comment.value } };
    console.log(data)
    this.rfqService.modify(data).then(v => {
      if (v.Status && v.Status == "Success") {
        this.isEditing = false;
       // this.selectedData = null;
        this.dataForm.disable();
        this.fetchRfq();
      }
    })
  }

  CanApprove() {
    if (!this.selectedData) {
      return false;
    }

    return this.permission.CanApproveRfq(this.selectedData.Entitlements)
  }

  CanReject() {
    if (!this.selectedData) {
      return false;
    }

    return this.permission.CanRejectRfq(this.selectedData.Entitlements)
  }

  CanModify() {
    if (!this.selectedData) {
      return false;
    }

    return this.permission.CanEditRfq(this.selectedData.Entitlements)
  }




}
