import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfqActionComponent } from './rfq-action.component';

describe('RfqActionComponent', () => {
  let component: RfqActionComponent;
  let fixture: ComponentFixture<RfqActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfqActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfqActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
