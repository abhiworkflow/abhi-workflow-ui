import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RouteUrls } from 'src/app/misc/constants';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit {

  clientId: string = null;
  clientDetail : any = null;

  constructor(private router: Router, private route: ActivatedRoute) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
  }

  ngOnInit() {
    if(this.clientId)
      this.getClientDetails();
  }

  getClientDetails() {
    console.log("server call");
    this.clientDetail = {
      'name': "company"
    };
    
  }

  createQuote() {
    if(this.clientId)
      this.router.navigate([RouteUrls.createQuote, this.clientId]);
  }
}
