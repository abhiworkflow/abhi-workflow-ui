import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { RouteUrls } from 'src/app/misc/constants';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.scss']
})
export class CreateClientComponent implements OnInit {

  @ViewChild('clientIdModal')
  clientIdModal: ModalDirective;

  tab1Lable = "Client Information";

  count = 1;

  buttonText: string = "Next";

  clientInformation1form: FormGroup;
  clientInformation2form: FormGroup;
  clientInformation3form: FormGroup;

  dropdownlist:any[] = [
    'a','b','c'
  ];

  clientId: string = null;

  constructor(public fb: FormBuilder, private router: Router) {
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    this.clientInformation1form = this.fb.group({
      customer_type: [''],
      govt_undertaking: [''],
      eta_account_available: [''],
      apply_for_eta_account: [''],
      salutation: [''],
      pincode: ['', Validators.required],
      name_of_company: ['', Validators.required],
      company_type: [''],
      date_of_incorporation: [''],
      industry_type: [''],
      nationality: [''],
      service_tax_registration_no: [''],
      tan_no: [''],
      company_registration_no: [''],
      is_abg_policy: [''],
      do_you_have_pan: [''],
      pan_number: ['']
    });

    this.clientInformation2form = this.fb.group({
      primary_email_id: [''],
      contact_person: [''],
      client_mobile_number: [''],
      std_landline_no: [''],
      start_date: [''],
      end_date: [''],
      others: [''],
      remarks: [''],
      status: [''],
      gst_registration_status: [''],
      sun_sidary: [''],
      state_gst: [''],
      pan_number_gst: [''],
      series_number_gst: [''],
      go_green: [''],
      note: [''],
    });

    this.clientInformation3form = this.fb.group({
      is_parent_child_available: [''],
      select_tagging: [''],
      tag_parent_child_id: ['']
    });
  }

  getFormControl(controlName: string): AbstractControl {
      if(this.count == 1)
        return this.clientInformation1form.get(controlName);
  }

  buttonNext() {

    // Form Value Get
    let objectValue = null;
    if(this.count == 1) {
      if(this.clientInformation1form.valid)
        objectValue = this.clientInformation1form.value;
      else {
        Object.keys(this.clientInformation1form.controls).forEach(field => {
          const control = this.clientInformation1form.get(field);
          control.markAsTouched({ onlySelf: true });
        });
        objectValue = null;
      }
    } else if(this.count == 2)
      objectValue = this.clientInformation2form.value;
    else if(this.count == 3)
      objectValue = this.clientInformation3form.value;
    
    console.log(objectValue);
    if(objectValue) {
      if (this.count < 3) {
        this.count += 1;
        if (this.count == 3)
          this.buttonText = "Generate Client ID"
      } else {
        this.clientId = "223232";
        this.showClientIdModal();
      }
    }
  }

  showClientIdModal() {
    this.clientIdModal.show();
  }

  hideClientIdModal() {
    this.clientIdModal.hide();
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }
}
