import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouteUrls } from 'src/app/misc/constants';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  clientId: string = null;
  clientName: string = null;
  pan: string = null;
  pincode: string = null;

  clientList: any[] = [];

  private gridApi;
  private gridColumnApi;

  public rowSelection = "single";

  columnDefs = [
    {headerName: 'Client Id', field: 'client_id' },
    {headerName: 'PAN', field: 'pan' },
    {headerName: 'Client Name', field: 'client_name' },
    {headerName: 'PINCODE', field: 'pincode' },
  ];

  rowData = [];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  searchButton() {
    console.log(this.clientId);
    console.log(this.clientName);
    console.log(this.pan);
    console.log(this.pincode);
    // Server call 
    this.clientList = [ 
      { client_id: 'Toyota', pan: 'pan222', pincode: 35000, client_name: "ss" },
      { client_id: 'Ford', pan: 'pan222', pincode: 32000, client_name: "ss"},
      { client_id: 'Porsche', pan: 'pan222', pincode: 72000, client_name: "ss" }
    ];
    this.rowData = this.clientList;
  }

  onSelectionChanged() {
    var selectedRows = this.gridApi.getSelectedRows();
    console.log("onSelectionChanged");
    console.log(selectedRows);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  createNew() {
    this.router.navigate([RouteUrls.createClient]);
  }
}
