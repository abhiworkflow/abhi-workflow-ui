import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { Input } from '@angular/core';

@Component({
  selector: 'app-risk-details',
  templateUrl: './risk-details.component.html',
  styleUrls: ['./risk-details.component.scss']
})
export class RiskDetailsComponent implements OnInit {

  @Input()
  rDCount: number = 1;

  @ViewChild('addDemographyModal')
  addDemographyModal: ModalDirective;

  @ViewChild('uploadMannualyModal')
  uploadMannualyModal: ModalDirective;

  dropdownlist:any[] = [
    'a','b','c'
  ];

  riskDetailsForm: FormGroup;
  
  constructor(public fb: FormBuilder) { 
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    this.riskDetailsForm = this.fb.group({
      is_member_data_available: [''],
      proposal_basis:['', Validators.required],
      industry_type:[''],
      insured_person_relationship:['', Validators.required],
      description:[''],
      tax_type:[''],
      corporate_floater:['', Validators.required],
      family_unit_definition:[''],
      premium_calculation_basis:['', Validators.required],
      credit_limit:[''],
      free_look_period:['']
    });
  }
  
  getFormControl(controlName: string): AbstractControl {
    return this.riskDetailsForm.get(controlName);
  }

  // Add definition
  definitionAdd() {
    this.showAddDemographyModal();
  }

  // Show addDemographyModal
  showAddDemographyModal() {
    this.addDemographyModal.show();
  }

  // Hide addDemographyModal
  hideAddDemographyModal() {
    this.addDemographyModal.hide();
  }

  // Upload Mannualy Button
  uploadMannualyButton(){
    this.hideAddDemographyModal();
    this.showUploadMannualyModal();
  }

  // Show uploadMannualyModal
  showUploadMannualyModal() {
    this.uploadMannualyModal.show();
  }

  // Hide uploadMannualyModal
  hideUploadMannualyModal() {
    this.uploadMannualyModal.hide();
  }

  getData() {
    let objectValue = null;

    switch (this.rDCount) {
      case 1:
        if (this.riskDetailsForm.valid) {
          objectValue = { "risk_details" : this.riskDetailsForm.value };
        } else {
          Object.keys(this.riskDetailsForm.controls).forEach(field => {
            const control = this.riskDetailsForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 2:
        objectValue = { "risk_details" : this.riskDetailsForm.value };
        break;
      case 3:
        objectValue = { "risk_details" : this.riskDetailsForm.value };
        break;
    }

    return objectValue;
  }
}
