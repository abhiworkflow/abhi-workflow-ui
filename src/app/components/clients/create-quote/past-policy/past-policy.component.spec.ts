import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastPolicyComponent } from './past-policy.component';

describe('PastPolicyComponent', () => {
  let component: PastPolicyComponent;
  let fixture: ComponentFixture<PastPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
