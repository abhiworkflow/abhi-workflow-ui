import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-past-policy',
  templateUrl: './past-policy.component.html',
  styleUrls: ['./past-policy.component.scss']
})
export class PastPolicyComponent implements OnInit {

  @Input()
  pPCount: number = 1;
  
  dropdownlist:any[] = [
    'a','b','c'
  ];
  
  pastPolicyForm: FormGroup;
  
  constructor(public fb: FormBuilder) { 
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    this.pastPolicyForm = this.fb.group({
      client_code_name: [''],
      gstin:[''],
      email:['']
    });
  }

  getData() {
    let objectValue = null;

    switch (this.pPCount) {
      case 1:
        if (this.pastPolicyForm.valid) {
          objectValue = { "past_policy" : this.pastPolicyForm.value };
        } else {
          Object.keys(this.pastPolicyForm.controls).forEach(field => {
            const control = this.pastPolicyForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 2:
        objectValue = { "past_policy" : this.pastPolicyForm.value };
        break;
      case 3:
        objectValue = { "past_policy" : this.pastPolicyForm.value };
        break;
    }

    return objectValue;
  }

}
