import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralProposalComponent } from './general-proposal.component';

describe('GeneralProposalComponent', () => {
  let component: GeneralProposalComponent;
  let fixture: ComponentFixture<GeneralProposalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralProposalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralProposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
