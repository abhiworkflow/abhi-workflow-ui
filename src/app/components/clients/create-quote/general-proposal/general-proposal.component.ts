import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-general-proposal',
  templateUrl: './general-proposal.component.html',
  styleUrls: ['./general-proposal.component.scss']
})
export class GeneralProposalComponent implements OnInit {

  @Input()
  gPCount: number = 1;
  
  dropdownlist:any[] = [
    'a','b','c'
  ];

  clientInformationForm: FormGroup;
  businessInformationForm: FormGroup;
  proposalInformationForm: FormGroup;
  
  constructor(public fb: FormBuilder) { 
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    // General Proposal Form
    this.clientInformationForm = this.fb.group({
      client_code_name: [''],
      gstin:[''],
      dispatch_mode:[''],
      email:[''],
      previous_cancelled:[''],
      reason: [''],
      process_remarks: [''],
      business_type: ['', Validators.required],
      transaction_type: [''],
      product_name_code: [''],
      plan_scheme: [''],
      policy_variant: [''],
      document_control_no:[''],
      cover_type:['']
    });

    // Business Information Form
    this.businessInformationForm = this.fb.group({
      office_code_location: [''],
      business_source_name: ['', Validators.required],
      sales_manager_name_code: [''],
      intermediary_channel_name_code: ['', Validators.required],
      business_channel_type: [''],
      business_servicing_channel_type: [''],
      business_channel_branch: [''],
      business_servicing_channel: [''],
      brokerage_commision_payable: ['', Validators.required],
      share: [''],
      is_multiple_intermediary:[''],
      branch_reference_id: [''],
      sector: [''],
      bank_branch_name: ['']
    });

    this.proposalInformationForm = this.fb.group({
      proposal_received_date_by_ops:[''],
      proposal_received_time_by_ops:[''],
      proposal_issue_date:[''],
      proposal_issue_Time:[''],
      policy_start_date:['', Validators.required],
      policy_start_time:[''],
      policy_end_date:[''],
      policy_end_time:[''],
      policy_tenure:[''],
      is_proposal_issued:[''],
      proposal_number:[''],
      place_of_issue:[''],
      proposal_amount:[''],
      rural_urban:[''],
      option_for_calculation:[''],
      is_it_master_policy:[''],
      ri_inward:[''],
      is_it_pre_underwritten:[''],
      service_tax_exemption_category:[''],
      is_premium_config:[''],
      coinsurance_status:[''],
      is_banca_policy:[''],
      own_share:[''],
      sme_code:['', Validators.required],
      remarks:[''],
      condition_clauses :['']
    });
  }

  getFormControl(controlName: string): AbstractControl {
    switch (this.gPCount) {
      case 1:
        return this.clientInformationForm.get(controlName);
      case 2:
        return this.businessInformationForm.get(controlName);
      case 3:
        return this.proposalInformationForm.get(controlName);
    }
  }

  getData() {
    let objectValue = null;

    switch (this.gPCount) {
      case 1:
        if (this.clientInformationForm.valid) {
          objectValue = { "client_information" : this.clientInformationForm.value };
        } else {
          Object.keys(this.clientInformationForm.controls).forEach(field => {
            const control = this.clientInformationForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 2:
        if (this.businessInformationForm.valid) {
          objectValue = { "business_source_information" : this.businessInformationForm.value };
        } else {
          Object.keys(this.businessInformationForm.controls).forEach(field => {
            const control = this.businessInformationForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 3:
        if (this.proposalInformationForm.valid) {
          objectValue = { "proposal_information" : this.proposalInformationForm.value };
        } else {
          Object.keys(this.proposalInformationForm.controls).forEach(field => {
            const control = this.proposalInformationForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
    }

    return objectValue;
  }
}
