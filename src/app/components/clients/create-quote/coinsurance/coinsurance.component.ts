import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-coinsurance',
  templateUrl: './coinsurance.component.html',
  styleUrls: ['./coinsurance.component.scss']
})
export class CoinsuranceComponent implements OnInit {

  @Input()
  cCount: number = 1;
  
  dropdownlist:any[] = [
    'a','b','c'
  ];

  coinsuranceList: any[] = [];
  coinsuranceForm: FormGroup;

  // Ag-Grid Variable
  private gridApi;
  private gridColumnApi;
  public rowSelection = "single";

  columnDefs = [
    {headerName: 'S. No.', field: 's_no' },
    {headerName: 'Coinsurance Type', field: 'co_insurance_type' },
    {headerName: 'Coinsurer Type', field: 'co_insurer_type'},
    {headerName: 'Insurance Name', field: 'insurance_company'},
    {headerName: 'Commission', field: 'full_commission_leader'},
    {headerName: 'Service Tax', field: 's_tax_leader'},
    {headerName: 'Branch', field: 'branch'},
    {headerName: 'Office', field: 'office_code'},
    {headerName: 'Share(%)', field: 'share'},
    {headerName: 'Edit', field: 'edit'},
    {headerName: 'Delete', field: 'delete'},
  ];

  rowData = [];
  
  constructor(public fb: FormBuilder) { 
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    this.coinsuranceForm = this.fb.group({
      co_insurance_type: ['', Validators.required],
      unique_reference_code:[''],
      full_commission_leader:[''],
      s_tax_leader:[''],
      policy_number:[''],
      co_insurer_type:['', Validators.required],
      insurance_company:['', Validators.required],
      branch:[''],
      office_code:[''],
      address:[''],
      share:[''],
      administrative_charges:[''],
      administrative_charges_rate:[''],
      penal_charges:[''],
      penal_charges_rate:[''],
      other_remarks:['']
    });
  }

  submitButton() {
    if(this.coinsuranceForm.valid) {
      let editValue= this.coinsuranceForm.value;
      console.log(editValue);
      this.coinsuranceList.push(editValue);
      console.log(this.coinsuranceList);
      this.rowData = this.coinsuranceList;

      // Form Reset
      this.coinsuranceForm.reset();
    }
  }

  onSelectionChanged() {
    var selectedRows = this.gridApi.getSelectedRows();
    console.log("onSelectionChanged");
    console.log(selectedRows);

    // Edit Form
    this.setFormData(selectedRows[0]);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  // Ser Form Data
  setFormData(coinsValue: any) {
    this.coinsuranceForm.patchValue({
      co_insurance_type: coinsValue.co_insurance_type,
      unique_reference_code:coinsValue.unique_reference_code,
      full_commission_leader:coinsValue.full_commission_leader,
      s_tax_leader:coinsValue.s_tax_leader,
      policy_number:coinsValue.policy_number,
      co_insurer_type:coinsValue.co_insurer_type,
      insurance_company:coinsValue.insurance_company,
      branch:coinsValue.branch,
      office_code:coinsValue.office_code,
      address:coinsValue.address,
      share:coinsValue.share,
      administrative_charges:coinsValue.administrative_charges,
      administrative_charges_rate:coinsValue.administrative_charges_rate,
      penal_charges:coinsValue.penal_charges,
      penal_charges_rate:coinsValue.penal_charges_rate,
      other_remarks:coinsValue.other_remarks
    });
  }

  getData() {
    let objectValue = null;

    switch (this.cCount) {
      case 1:
        if (this.coinsuranceForm.valid) {
          objectValue = { "coinsurance" : this.coinsuranceForm.value };
        } else {
          Object.keys(this.coinsuranceForm.controls).forEach(field => {
            const control = this.coinsuranceForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 2:
        objectValue = { "coinsurance" : this.coinsuranceForm.value };
        break;
      case 3:
        objectValue = { "coinsurance" : this.coinsuranceForm.value };
        break;
    }

    return objectValue;
  }

}
