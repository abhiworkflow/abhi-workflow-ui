import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinsuranceComponent } from './coinsurance.component';

describe('CoinsuranceComponent', () => {
  let component: CoinsuranceComponent;
  let fixture: ComponentFixture<CoinsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
