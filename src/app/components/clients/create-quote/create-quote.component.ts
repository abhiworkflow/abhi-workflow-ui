import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { GeneralProposalComponent } from 'src/app/components/clients/create-quote/general-proposal/general-proposal.component';
import { CoinsuranceComponent } from 'src/app/components/clients/create-quote/coinsurance/coinsurance.component';
import { RiskDetailsComponent } from 'src/app/components/clients/create-quote/risk-details/risk-details.component';
import { PastPolicyComponent } from 'src/app/components/clients/create-quote/past-policy/past-policy.component';
import { PremiumComponent } from 'src/app/components/clients/create-quote/premium/premium.component';
import { ModalDirective } from 'ngx-bootstrap';
import { RouteUrls } from 'src/app/misc/constants';

@Component({
  selector: 'app-create-quote',
  templateUrl: './create-quote.component.html',
  styleUrls: ['./create-quote.component.scss']
})
export class CreateQuoteComponent implements OnInit {

  clientId: string = null;
  clientDetail : any = null;

  tab1Lable = "General Proposal";
  tab2Lable = "Coinsurance";
  tab3Lable = "Risk Details";
  tab4Lable = "Past Policy";
  tab5Lable = "Premium";

  activeTab: string = this.tab1Lable;

  gPCount = 1;
  cCount = 1;
  rDCount = 1;
  pPCount = 1;
  pCount = 1;

  @ViewChild('generalProposal')
  generalProposalComponent: GeneralProposalComponent;

  @ViewChild('coinsurance')
  coinsuranceComponent: CoinsuranceComponent;

  @ViewChild('riskDetails')
  riskDetailsComponent: RiskDetailsComponent;

  @ViewChild('pastPolicy')
  pastPolicyComponent: PastPolicyComponent;

  @ViewChild('premium')
  premiumComponent: PremiumComponent;
  
  quotatinNo: string = null;
  
  @ViewChild('quotatinNoModal')
  quotatinNoModal: ModalDirective;

  constructor(private router: Router, private route: ActivatedRoute) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
   }

  ngOnInit() {
    if(this.clientId)
      this.getClientDetails();
  }

  selectedTabChange(event) {
    console.log("selectedTabChange");
    console.log(event);
    this.activeTab =  event.tab.textLabel;
    console.log(this.activeTab);
  }

  getClientDetails() {
    console.log("server call");
    this.clientDetail = {
      'name': "company"
    };
  }

  backButton() {

  }

  nextButton() {
    console.log("nextButton");
    console.log(this.activeTab);
    if(this.activeTab == this.tab1Lable) {
      let obj = this.generalProposalComponent.getData();
      console.log(obj);
      if(obj) {
        if(this.gPCount < 3) {
          this.gPCount += 1;
        } else {
          console.log("goto secound tab");
          this.quotatinNo = "quotatinNo cdasdas";
          this.showQuotatinNoModal();
        }
      }
    } else if(this.activeTab == this.tab2Lable+"1") {
      let obj = this.coinsuranceComponent.getData();
      console.log(obj);
      
      console.log("goto three tab");
      
    } else if(this.activeTab == this.tab3Lable+"1") {
      let obj = this.riskDetailsComponent.getData();
      console.log(obj);
      if(this.rDCount < 3) {
        this.rDCount += 1;
      } else {
        console.log("goto four tab");
      }
    } else if(this.activeTab == this.tab4Lable+"1") {
      let obj = this.pastPolicyComponent.getData();
      console.log(obj);
      if(this.pPCount < 6) {
        this.pPCount += 1;
      } else {
        console.log("goto five tab");
      }
    } else if(this.activeTab == this.tab5Lable+"1") {
      let obj = this.premiumComponent.getData();
      console.log(obj);
      console.log("compalite ");
    }
  }

  showQuotatinNoModal() {
    this.quotatinNoModal.show();
  }

  hideQuotatinNoModal() {
    this.quotatinNoModal.hide();
  }

  goToClientPage() {
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }
}
