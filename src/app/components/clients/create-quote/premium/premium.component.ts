import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-premium',
  templateUrl: './premium.component.html',
  styleUrls: ['./premium.component.scss']
})
export class PremiumComponent implements OnInit {

  @Input()
  pCount: number = 1;
  
  premiumForm: FormGroup;
  
  constructor(public fb: FormBuilder) { 
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    this.premiumForm = this.fb.group({
      client_code_name: [''],
      gstin:[''],
      email:['']
    });
  }

  getData() {
    let objectValue = null;

    switch (this.pCount) {
      case 1:
        if (this.premiumForm.valid) {
          objectValue = { "premium" : this.premiumForm.value };
        } else {
          Object.keys(this.premiumForm.controls).forEach(field => {
            const control = this.premiumForm.get(field);
            control.markAsTouched({ onlySelf: true });
          });
          objectValue = null;
        }
        break;
      case 2:
        objectValue = { "premium" : this.premiumForm.value };
        break;
      case 3:
        objectValue = { "premium" : this.premiumForm.value };
        break;
    }

    return objectValue;
  }
}
