import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHcnComponent } from './create-hcn.component';

describe('CreateHcnComponent', () => {
  let component: CreateHcnComponent;
  let fixture: ComponentFixture<CreateHcnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHcnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHcnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
