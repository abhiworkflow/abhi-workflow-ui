import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-hcn',
  templateUrl: './create-hcn.component.html',
  styleUrls: ['./create-hcn.component.scss']
})
export class CreateHcnComponent implements OnInit {

  @ViewChild('previewModal')
  previewModal: ModalDirective;
  
  clientId: string = null;
  clientDetail : any = null;
  createHcnValue: string = null;

  tab1Lable = "Create HCN";
  tab2Lable = "Download";

  createHcnForm: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute,
    public fb: FormBuilder) {
    
    this.initializeForm();

    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
  }

  ngOnInit() {
    if(this.clientId)
      this.getClientDetails();
  }

  initializeForm() {
    this.createHcnForm = this.fb.group({
      date: [''],
      payment_date: ['', Validators.required],
      addrerss: ['', Validators.required],
      name_of_the_bank_and_branch: ['', Validators.required],
      subject: ['', Validators.required],
      reference_no: ['', Validators.required],
      policy_start_date: ['', Validators.required],
      policy_end_date: ['', Validators.required],
      amount_of_policy: ['', Validators.required],
    });
  }

  getFormControl(controlName: string): AbstractControl {
    return this.createHcnForm.get(controlName);
  }

  getClientDetails() {
    console.log("server call");
    this.clientDetail = {
      'name': "company"
    };
  }

  preview() {
    if(this.createHcnForm.valid) {
      console.log(this.createHcnForm.value);
      this.createHcnValue = this.createHcnForm.value;
      this.showPreviewModal();
    } else {
      Object.keys(this.createHcnForm.controls).forEach(field => {
        const control = this.createHcnForm.get(field);
        control.markAsTouched({ onlySelf: true });
      });
    }
    console.log("preview");
  }

  goToClientHome() {
    console.log("goToClientHome");
  }

  showPreviewModal() {
    this.previewModal.show();
  }

  hidePreviewModal() {
    this.createHcnValue = null;
    this.previewModal.hide();
  }
}
