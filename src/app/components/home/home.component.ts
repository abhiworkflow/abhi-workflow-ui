import { Component, OnInit } from '@angular/core';
import { PermissionService } from 'src/app/services/permission.service';
import { RouteUrls } from 'src/app/misc/constants';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  user = "";
  addRfqLink = `../${RouteUrls.addRfq}`;
  viewRfqLink = `../${RouteUrls.viewRfq}`;
  homeRfqLink =  `../${RouteUrls.home}`;

  constructor(private router: Router,private permission: PermissionService, private authService : AuthService) { }

  ngOnInit() {
    this.user = this.authService.user;
  }

  signout(){
    this.authService.signOut();
    this.router.navigate([RouteUrls.signin]);
  }

  canAddRfq() {
    return this.permission.canAddRfq();
  }

  

}
