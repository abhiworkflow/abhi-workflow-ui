import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  @Input()
  messageTitle: string = null;

  @Input()
  buttonText: string = null;

  @Output()
  onClickOutput = new EventEmitter<any>();
  
  @ViewChild('successModal')
  successModal: ModalDirective;

  constructor() { }

  ngOnInit() {
  }

  showSuccessModal() {
    this.successModal.show();
  }

  hideSuccessModal() {
    this.successModal.hide();
    this.onClickOutput.emit(null);
  }

}
