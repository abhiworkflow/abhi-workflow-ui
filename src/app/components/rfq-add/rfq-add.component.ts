import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RfqService } from 'src/app/services/rfq.service';
import { RouteUrls } from 'src/app/misc/constants';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rfq-add',
  templateUrl: './rfq-add.component.html',
  styleUrls: ['./rfq-add.component.scss']
})
export class RfqAddComponent implements OnInit {
  viewRfqLink = `../${RouteUrls.viewRfq}`;
  rfqForm: FormGroup;
  homeRfqLink = `../${RouteUrls.home}`;
  showStatus = false;
  status = "";
  user = "";
  showError = false;


  constructor(private router: Router,fb: FormBuilder, private rfqService: RfqService, private authService : AuthService) {


    this.rfqForm = fb.group({
      companyName: [''],
      companyAddress: [''],
      empCount: [''],
      amount: ['']
    });

  }

  ngOnInit() {
    this.user = this.authService.user;
  }


  signout(){
    this.authService.signOut();
    this.router.navigate([RouteUrls.signin]);
  }

  submit() {
    const { companyName: CompanyName, companyAddress: CompanyAddress, empCount: EmpCount, amount: Amount } = this.rfqForm.value;
    this.rfqService.uploadRfq({ Data: { CompanyName, CompanyAddress, EmpCount, Amount } })
      .then(v => {
        if (v.Status == "Success") {
          this.showStatus = true;
          this.showError = false;
          this.status = "Rfq uploaded successfully!!";
          this.rfqForm.reset();
        } else {
          this.showStatus = false;
          this.showError = true;
          this.status = "Rfq could not be created...";
        }
      }).catch(e => {

      });
  }

}
