import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfqAddComponent } from './rfq-add.component';

describe('RfqAddComponent', () => {
  let component: RfqAddComponent;
  let fixture: ComponentFixture<RfqAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfqAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfqAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
