import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePremiumComponent } from './manage-premium.component';

describe('ManagePremiumComponent', () => {
  let component: ManagePremiumComponent;
  let fixture: ComponentFixture<ManagePremiumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePremiumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePremiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
