import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { RouteUrls } from 'src/app/misc/constants';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-manage-premium',
  templateUrl: './manage-premium.component.html',
  styleUrls: ['./manage-premium.component.scss']
})
export class ManagePremiumComponent implements OnInit {

  mPCount: number = 1;
  buttonTittle: string = "> Next";

  tab1Lable = "Manage Premium";
  tab2Lable = "Download";

  clientId: string = null;

  @ViewChild('successModal')
  successModal: ModalDirective;

  @ViewChild('annexsureGeneratedModal')
  annexsureGeneratedModal: ModalDirective;

  // Grid 
  private gridApi;
  private gridColumnApi;

  columnDefs = [
    { headerName: 'S.no', field: 's_no' },
    { headerName: 'Receipt Number', field: 'receipt_number' },
    { headerName: 'Targeted Amount', field: 'trgeted_amount' },
    { headerName: 'Excess Amount', field: 'excess_amount' },
  ];

  rowData = [
    {
      s_no: 's_no', receipt_number: 'receipt_number', 
      trgeted_amount: 'trgeted_amount', excess_amount: "excess_amount"
    }
  ];

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  constructor(private router: Router, private route: ActivatedRoute) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
  }

  ngOnInit() {
  }

  buttonNext() {
    if (this.mPCount < 2) {
      this.mPCount += 1;
      if (this.mPCount == 2)
        this.buttonTittle = "> Submit";
    } else {
      this.showSuccessModal();
    }
  }

  showSuccessModal() {
    this.successModal.show();
  }

  hideSuccessModal() {
    this.successModal.hide();
  }

  generateAnnexsure() {
    this.hideSuccessModal();
    this.showAnnexsureGeneratedModal();
  }

  showAnnexsureGeneratedModal() {
    this.annexsureGeneratedModal.show();
  }

  hideAnnexsureGeneratedModal() {
    this.annexsureGeneratedModal.hide();
  }

  goToClientPage() {
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }

  downloadAnnexsure() {
    this.hideAnnexsureGeneratedModal();
  }
}
