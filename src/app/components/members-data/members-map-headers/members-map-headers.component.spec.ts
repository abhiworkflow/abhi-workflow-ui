import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersMapHeadersComponent } from './members-map-headers.component';

describe('MembersMapHeadersComponent', () => {
  let component: MembersMapHeadersComponent;
  let fixture: ComponentFixture<MembersMapHeadersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembersMapHeadersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersMapHeadersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
