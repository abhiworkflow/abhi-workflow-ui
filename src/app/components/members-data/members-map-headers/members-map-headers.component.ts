import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RouteUrls } from 'src/app/misc/constants';

@Component({
  selector: 'app-members-map-headers',
  templateUrl: './members-map-headers.component.html',
  styleUrls: ['./members-map-headers.component.scss']
})
export class MembersMapHeadersComponent implements OnInit {

  isPreviewFlag: boolean = false

  clientId: string = null;

  constructor(private router: Router, private route: ActivatedRoute) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
   }

  ngOnInit() {
  }

  preview() {
    this.isPreviewFlag = true;
    console.log("preview");
  }

  backButton() {
    this.isPreviewFlag = false;
  }

  uploadButton() {
    console.log("uploadButton");
    this.router.navigate([RouteUrls.membersData, this.clientId])
  }
  
}
