import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadFilesComponent } from '../upload-files/upload-files.component';
import { RouteUrls } from 'src/app/misc/constants';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-members-data',
  templateUrl: './members-data.component.html',
  styleUrls: ['./members-data.component.scss']
})
export class MembersDataComponent implements OnInit {

  isUploadFlag: boolean = true;

  tab1Lable = "Upload Member";
  tab2Lable = "Download";

  clientId: string = null;
  clientDetail: any = null;

  // File Upload
  @ViewChild('uploadFileModal')
  uploadFileModal: UploadFilesComponent;

  @ViewChild('successModal')
  successModal: ModalDirective;

  constructor(private router: Router, private route: ActivatedRoute,
    public fb: FormBuilder) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
  }

  ngOnInit() {
    if (this.clientId)
      this.getClientDetails();
  }

  getClientDetails() {
    console.log("server call");
    this.clientDetail = {
      'name': "company"
    };
  }

  goToClientDetails() {

  }

  submit() {
    this.showSuccessModal();
  }

  fileUpload() {
    console.log("fileUpload");
    this.uploadFileModal.showModal();
    this.uploadFileModal.showModal().then(v => {

      if (v == null) {
        return "";
      }

      if (v.length > 0) {
        v.forEach(element => {
          //this.uploadFile(element);
        });
      } else {
        //this.uploadFile(v);
      }

      console.log(v);
      console.log("server call");
      this.goToMembersMapHeaders();
    }).catch(err => { });
  }

  goToMembersMapHeaders() {
    console.log("goToMembersMapHeaders");
    this.router.navigate([RouteUrls.membersMapHeaders, this.clientId]);
  }

  goToClientPage() {
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }

  showSuccessModal() {
    this.successModal.show();
  }

  hideSuccessModal() {
    this.successModal.hide();
  }

  checkButton() {
    this.hideSuccessModal();
    this.isUploadFlag = false;
  }

  // Grid 
  private gridApi;
  private gridColumnApi;

  columnDefs = [
    { headerName: 'S.no', field: 's_no' },
    { headerName: 'Group', field: 'group' },
    { headerName: 'Primary member', field: 'primary_member' },
    { headerName: 'Dependents', field: 'dependents' },
    { headerName: 'Total Members', field: 'total_members' },
    { headerName: 'Group family type', field: 'group_family_type' },
    { headerName: 'Risk category', field: 'risk_category' },
    { headerName: 'Sum Insured per member', field: 'sum_insured_per_member' },
    { headerName: 'Total Sum Insured(Group)', field: 'total_sum_insured_group' },
  ];

  rowData = [
    {
      s_no: 's_no', group: 'group', primary_member: 'primary_member', dependents: "dependents",
      total_members: "total_members", group_family_type: 'group_family_type', risk_category: 'risk_category',
      sum_insured_per_member: 'sum_insured_per_member', total_sum_insured_group: 'total_sum_insured_group'
    }
  ];

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  managePremium() {
    this.router.navigate([RouteUrls.managePremium, this.clientId]);
  }
}
