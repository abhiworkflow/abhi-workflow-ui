import { Component, OnInit, ViewChild } from '@angular/core';
import { RouteUrls } from 'src/app/misc/constants';
import { UploadFilesComponent } from '../upload-files/upload-files.component';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-e-card',
  templateUrl: './e-card.component.html',
  styleUrls: ['./e-card.component.scss']
})
export class ECardComponent implements OnInit {

  tab1Lable = "Create E Card";
  tab2Lable = "Download";

  clientId: string = null;
  clientDetail: any = null;

  eCaedType: string = null;

  eCaedTypeList: any[] = [
    'Per Policy',
    'Per Member',
    'Per Family'
  ];

  // File Upload
  @ViewChild('uploadFileModal')
  uploadFileModal: UploadFilesComponent;

  @ViewChild('successModal')
  successModal: ModalDirective;

  constructor(private router: Router, private route: ActivatedRoute) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
  }

  ngOnInit() {
    if (this.clientId)
      this.getClientDetails();
  }

  getClientDetails() {
    console.log("server call");
    this.clientDetail = {
      'name': "company"
    };
  }

  goToClientPage() {
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }

  generateECard() {
    this.showSuccessModal();
  }

  fileUpload() {
    console.log("fileUpload");
    this.uploadFileModal.showModal();
    this.uploadFileModal.showModal().then(v => {

      if (v == null) {
        return "";
      }

      if (v.length > 0) {
        v.forEach(element => {
          //this.uploadFile(element);
        });
      } else {
        //this.uploadFile(v);
      }

      console.log(v);
      console.log("server call");
    }).catch(err => { });
  }

  generateFromSystem() {
    console.log("generateFromSystem");
  }

  showSuccessModal() {
    this.successModal.show();
  }

  hideSuccessModal() {
    this.successModal.hide();
  }

  reviewButton() {
    this.hideSuccessModal();
    console.log("reviewButton");
  }

}
