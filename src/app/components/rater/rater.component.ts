import { Component, OnInit, ViewChild } from '@angular/core';
import { UploadFilesComponent } from '../upload-files/upload-files.component';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteUrls } from 'src/app/misc/constants';
import { ModalDirective } from 'ngx-bootstrap';
import { PopupComponent } from '../popup/popup.component';
@Component({
  selector: 'app-rater',
  templateUrl: './rater.component.html',
  styleUrls: ['./rater.component.scss']
})
export class RaterComponent implements OnInit {

  tab1Lable = "Upload Rater";
  tab2Lable = "Download";

  clientId: string = null;
  clientDetail: any = null;

  raterType: string = null;

  raterTypeList: any[] = [
    'a',
    'b',
    'c'
  ]

  // File Upload
  @ViewChild('uploadFileModal')
  uploadFileModal: UploadFilesComponent;

  @ViewChild('generateRaterModal')
  generateRaterModal: ModalDirective;

  @ViewChild('policyTypeModal')
  policyTypeModal: ModalDirective;

  @ViewChild('gpaModal')
  gpaModal: ModalDirective;

  @ViewChild('ghiModal')
  ghiModal: ModalDirective;

  @ViewChild('successModal')
  successModal: PopupComponent;

  constructor(private router: Router, private route: ActivatedRoute) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
  }

  ngOnInit() {
    if (this.clientId)
      this.getClientDetails();
  }

  getClientDetails() {
    console.log("server call");
    this.clientDetail = {
      'name': "company"
    };
  }

  goToClientPage() {
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }

  submitRater() {
    this.showSuccessModal();
  }

  fileUpload() {
    console.log("fileUpload");
    this.uploadFileModal.showModal();
    this.uploadFileModal.showModal().then(v => {

      if (v == null) {
        return "";
      }

      if (v.length > 0) {
        v.forEach(element => {
          //this.uploadFile(element);
        });
      } else {
        //this.uploadFile(v);
      }

      console.log(v);
      console.log("server call");
    }).catch(err => { });
  }

  generateRater() {
    this.showGenerateRaterModal();
  }

  // Generate Rater Modal
  showGenerateRaterModal() {
    this.generateRaterModal.show();
  }

  hideGenerateRaterModal() {
    this.generateRaterModal.hide();
  }

  previewButton() {
    this.hideGenerateRaterModal();
    this.showPolicyTypeModal();
  }

  // PolicyType Modal
  showPolicyTypeModal() {
    this.policyTypeModal.show();
  }

  hidePolicyTypeModal() {
    this.policyTypeModal.hide();
  }

  gpaButton() {
    this.hidePolicyTypeModal();
    this.showGpaModal();
  }

  ghiButton() {
    this.hidePolicyTypeModal();
    this.showGhiModal();
  }

  // GpaModal
  showGpaModal() {
    this.gpaModal.show();
  }

  hideGpaModal() {
    this.gpaModal.hide();
  }

  gpaUploadRater() {
    this.hideGpaModal();
  }

  // GiModal
  showGhiModal() {
    this.ghiModal.show();
  }

  hideGhiModal() {
    this.ghiModal.hide();
  }

  ghiUploadRater() {
    this.hideGhiModal();
  }

  // SuccessModal
  showSuccessModal() {
    this.successModal.showSuccessModal();
  }

  hideSuccessModal(event) {
    this.goToClientPage();
  }
}
