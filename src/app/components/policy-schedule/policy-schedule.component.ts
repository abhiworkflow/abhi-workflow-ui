import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { RouteUrls } from 'src/app/misc/constants';

@Component({
  selector: 'app-policy-schedule',
  templateUrl: './policy-schedule.component.html',
  styleUrls: ['./policy-schedule.component.scss']
})
export class PolicyScheduleComponent implements OnInit {

  buttonTittle: string = "> Next";

  cPSCount: number = 1;
  tab1Lable = "Create Policy Schedule";
  tab2Lable = "Download";

  clientId: string = null;

  @ViewChild('successModal')
  successModal: ModalDirective;

  constructor(private router: Router, private route: ActivatedRoute) {
    // Get client ID
    this.route.paramMap.subscribe(params => {
      if (params) {
        this.clientId = params.get('id');
      }
    });
  }

  ngOnInit() {
    if (this.clientId)
      this.getClientDetails();
  }

  getClientDetails() {
    console.log("server call");
  }

  editButton() {

  }

  nextButton() {
    if (this.cPSCount < 7) {
      this.cPSCount += 1;
      if (this.cPSCount == 7)
        this.buttonTittle = "> Submit";
    } else {
      this.showSuccessModal();
    }
  }

  showSuccessModal() {
    this.successModal.show();
  }

  hideSuccessModal() {
    this.successModal.hide();
  }

  goToClientPage() {
    this.router.navigate([RouteUrls.clientDetails, this.clientId]);
  }

  reviewButton() {
    console.log("review");
    this.hideSuccessModal();
  }
}
