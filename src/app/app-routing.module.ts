import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth-guard.service';

import { SigninComponent } from './components/signin/signin.component';
import { RfqActionComponent } from './components/rfq-action/rfq-action.component';
import { RfqAddComponent } from './components/rfq-add/rfq-add.component';
import { HomeComponent } from './components/home/home.component';
import { NotAuthorizedComponent } from './components/not-authorized/not-authorized.component';
import { ProfileGuard } from './services/profile-guard.service';
import { RouteUrls } from './misc/constants';
import { NotificationsComponent } from 'src/app/components/notifications/notifications.component';
import { BusinessIntimationComponent } from 'src/app/components/business-intimation/business-intimation.component';
import { ClientsComponent } from 'src/app/components/clients/clients.component';
import { CreateClientComponent } from 'src/app/components/clients/create-client/create-client.component';
import { ClientDetailsComponent } from 'src/app/components/clients/client-details/client-details.component';
import { CreateQuoteComponent } from 'src/app/components/clients/create-quote/create-quote.component';
import { CreateHcnComponent } from 'src/app/components/clients/create-hcn/create-hcn.component';
import { ManagePremiumComponent } from './components/manage-premium/manage-premium.component';
import { MembersDataComponent } from './components/members-data/members-data.component';
import { MembersMapHeadersComponent } from './components/members-data/members-map-headers/members-map-headers.component';
import { RaterComponent } from './components/rater/rater.component';
import { PolicyScheduleComponent } from './components/policy-schedule/policy-schedule.component';
import { ECardComponent } from './components/e-card/e-card.component';

const routes: Routes = [
  { path: '', redirectTo: RouteUrls.signin, pathMatch: 'full' },
  { path: RouteUrls.signin, component: SigninComponent  },
  { path: RouteUrls.home, component: HomeComponent, canActivate:[AuthGuard]  },
  { path: RouteUrls.viewRfq, component: RfqActionComponent, canActivate:[AuthGuard, ProfileGuard] },
  { path: RouteUrls.addRfq, component: RfqAddComponent, canActivate:[AuthGuard, ProfileGuard] },  
  { path: RouteUrls.notifications, component: NotificationsComponent},
  { path: RouteUrls.businessIntimation, component: BusinessIntimationComponent},
  { path: RouteUrls.clients, component: ClientsComponent},
  { path: RouteUrls.createClient, component: CreateClientComponent},
  { path: RouteUrls.clientDetails+'/:id', component: ClientDetailsComponent},
  { path: RouteUrls.createQuote+'/:id', component: CreateQuoteComponent},
  { path: RouteUrls.notauthorized, component: NotAuthorizedComponent},
  { path: RouteUrls.createHcn+'/:id', component: CreateHcnComponent},
  { path: RouteUrls.membersData+'/:id', component: MembersDataComponent},
  { path: RouteUrls.managePremium+'/:id', component: ManagePremiumComponent},
  { path: RouteUrls.rater+'/:id', component: RaterComponent},
  { path: RouteUrls.membersMapHeaders+'/:id', component: MembersMapHeadersComponent},
  { path: RouteUrls.policySchedule+'/:id', component: PolicyScheduleComponent},
  { path: RouteUrls.eCard+'/:id', component: ECardComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
