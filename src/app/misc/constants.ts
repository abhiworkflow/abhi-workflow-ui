export const RouteUrls = {

    signin: "sign-in",
    home: 'home',
    addRfq: 'add-rfq',
    viewRfq: 'view-rfq',
    notifications: 'notifications',
    businessIntimation: 'business-intimation',
    clients: 'clients',
    createClient: 'create-client',
    clientDetails: 'client-details',
    createQuote: 'create-quote',
    createHcn: 'create-hcn',
    membersData: 'members-data',
    managePremium: 'manage-premium',
    membersMapHeaders: 'members-map-headers',
    rater: 'rater',
    policySchedule: 'policy-schedule',
    eCard: 'e-card',
    notauthorized: 'notauthorized'
}

export const PermissionTypes = {

    Rfq_CanAdd: "Rfq_CanAdd",
    Rfq_CanView: 'Rfq_CanView',
    Rfq_CanApprove: "Rfq_CanApprove",
    Rfq_CanReject: "Rfq_CanReject",
    Rfq_CanEdit : "Rfq_CanEdit"
}

export const machineUrl = "http://localhost:61254";