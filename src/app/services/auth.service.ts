import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PermissionTypes, machineUrl } from '../misc/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userEnts: string[] = [];
  roles: string[] = [];
  user = "";

  public isLoggedIn = false;
  public userId = "";
  constructor(private httpClient: HttpClient) { }


  signIn(username, password): Promise<any> {
    const ret = this.httpClient.post(`${machineUrl}/api/auth/login`, {
      "user_name": username,
      password
    }).toPromise<any>()


    ret.then(v => {

      if (v.IsValidUser) {
        this.isLoggedIn = true;
        this.user = username;
        this.userId = v.UserId;
        this.userEnts = v.Permission.Entitlements;
        this.roles = v.Permission.Roles;
      }
    })

    return ret;

  }

  signOut() {

  }

}
