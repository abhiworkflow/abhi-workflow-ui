import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { PermissionService } from './permission.service';
import { RouteUrls } from '../misc/constants';

@Injectable({
    providedIn: 'root'
  })
export class ProfileGuard implements CanActivate {    

    constructor(private permission: PermissionService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      
        if (!this.permission.canActivate(state.url)) {
            return this.router.parseUrl(`/${RouteUrls.notauthorized}`);
        }
        return true;
    }

}