import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PermissionTypes, machineUrl } from '../misc/constants';

@Injectable({
  providedIn: 'root'
})
export class RfqService {

  constructor(private httpClient: HttpClient) { }


  uploadRfq(data) {

    return this.httpClient.post(`${machineUrl}/api/rfq/add`, data).toPromise<any>();

  }


  getAllRfqs() {
    return this.httpClient.get(`${machineUrl}/api/rfq/quotes`).toPromise<any>();
  }

  approve(data) {
    return this.httpClient.post(`${machineUrl}/api/rfq/approve`, data).toPromise<any>();
  }

  reject(data) {
    return this.httpClient.post(`${machineUrl}/api/rfq/reject`, data).toPromise<any>();
  }

  modify(data) {
    return this.httpClient.post(`${machineUrl}/api/rfq/update`,  data ).toPromise<any>();
  }

}
