import { Injectable } from '@angular/core';
import { DataService } from 'src/app/services/data-services/data.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(public dataService: DataService) { }

  getNotifications() {
    const promise = new Promise((resolve, reject) => {
      this.dataService.getNotifications().then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }
}
