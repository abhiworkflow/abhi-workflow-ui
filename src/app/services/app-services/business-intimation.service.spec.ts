import { TestBed } from '@angular/core/testing';

import { BusinessIntimationService } from './business-intimation.service';

describe('BusinessIntimationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusinessIntimationService = TestBed.get(BusinessIntimationService);
    expect(service).toBeTruthy();
  });
});
