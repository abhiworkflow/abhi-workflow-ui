import { Injectable } from '@angular/core';
import { DataService } from 'src/app/services/data-services/data.service';

@Injectable({
  providedIn: 'root'
})
export class QuoteService {

  constructor(public dataService: DataService) { }

  getQuoteDetails(business_id: number) {
    const promise = new Promise((resolve, reject) => {
      this.dataService.getQuoteDetails(business_id).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createQuote(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createQuote(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

}
