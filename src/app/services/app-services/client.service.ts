import { Injectable } from '@angular/core';
import { DataService } from 'src/app/services/data-services/data.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(public dataService: DataService) { }

  getClientDetails(client_id: number) {
    const promise = new Promise((resolve, reject) => {
      this.dataService.getClientDetails(client_id).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createClientInformation1form(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createClientInformation1form(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createClientInformation2form(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createClientInformation2form(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createClientInformation3form(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createClientInformation3form(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }
}
