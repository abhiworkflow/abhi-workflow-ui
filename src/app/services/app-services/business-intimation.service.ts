import { Injectable } from '@angular/core';
import { DataService } from 'src/app/services/data-services/data.service';
@Injectable({
  providedIn: 'root'
})
export class BusinessIntimationService {

  constructor(public dataService: DataService) { }

  getBusinessIntimation(business_id: number) {
    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(business_id).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createGeneralInformation(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createPolicyHolderDetails(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createGstPaymentDetails(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createAbhiIntermediaryDetails(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createTpaBusinessSegmentDetails(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createPolicyDetails(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }

  createPolicyServicing(data: any): Promise<any> {
    let reuqestDataObj = {
      'data': data
    };

    const promise = new Promise((resolve, reject) => {
      this.dataService.createGeneralInformation(reuqestDataObj).then(result => {
        resolve(result);
      }).catch(e => { reject(e); });
    });

    return promise;
  }
}
