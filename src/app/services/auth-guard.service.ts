import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { RouteUrls } from '../misc/constants';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (!this.authService.isLoggedIn) {
            return this.router.createUrlTree([`/${RouteUrls.signin}`], { queryParams: { redirectUrl: state.url } });
        }
        return true;
    }

}