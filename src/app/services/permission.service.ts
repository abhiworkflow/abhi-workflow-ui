import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { RouteUrls, PermissionTypes } from '../misc/constants';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(private authService: AuthService) { }

  canAddRfq() {

    return this.verifyProfileEnts(PermissionTypes.Rfq_CanAdd);
  }

  canActivate(url) {
    let newUrl = url.toString();
    newUrl = newUrl.startsWith("/") ? newUrl.substring(1) : newUrl;

    switch (newUrl) {
      case RouteUrls.signin:
        return true;
      case RouteUrls.addRfq:
        return this.canAddRfq();

      case RouteUrls.viewRfq:
        return true;

      case RouteUrls.home:
        return true;

      case RouteUrls.notauthorized:
        return true;

      default:
        return false;
    }

  }


  CanApproveRfq(ents){
    return this.verifyDataEnts(ents, PermissionTypes.Rfq_CanApprove);
  }

  CanEditRfq(ents){
    return this.verifyDataEnts(ents, PermissionTypes.Rfq_CanEdit);
  }

  CanRejectRfq(ents){
    return this.verifyDataEnts(ents, PermissionTypes.Rfq_CanReject);
  }

  verifyProfileEnts(...requiredEnts: string[]) {
    return this.authService.userEnts.some(e => requiredEnts.some(re => e == re));
  }

  verifyDataEnts(dataEnts : string[], ...requiredEnts: string[]) {
    return dataEnts.some(e => requiredEnts.some(re => e == re));
  }

}
