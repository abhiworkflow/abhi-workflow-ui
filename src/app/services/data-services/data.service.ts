import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    apiUrl = "localhost"
    constructor(private httpClient: HttpClient) { }

    // Business intimation Service 
    getBusinessIntimation(id): Promise<any> {
        return this.httpClient.get(this.apiUrl + "/" + id).toPromise();
    }

    createGeneralInformation(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createPolicyHolderDetails(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createGstPaymentDetails(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createAbhiIntermediaryDetails(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createTpaBusinessSegmentDetails(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createPolicyDetails(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createPolicyServicing(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    // Client Service
    getClientDetails(id): Promise<any> {
        return this.httpClient.get(this.apiUrl + "/" + id).toPromise();
    }

    createClientInformation1form(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createClientInformation2form(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    createClientInformation3form(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    // Quote Service
    getQuoteDetails(id): Promise<any> {
        return this.httpClient.get(this.apiUrl + "/" + id).toPromise();
    }

    createQuote(objData): Promise<any> {
        return this.httpClient.post(this.apiUrl, objData).toPromise();
    }

    // Notification Service
    getNotifications(): Promise<any> {
        return this.httpClient.get(this.apiUrl).toPromise();
    }
}
