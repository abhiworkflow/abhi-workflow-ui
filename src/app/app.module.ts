import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './components/signin/signin.component';
import { RfqActionComponent } from './components/rfq-action/rfq-action.component';
import { RfqAddComponent } from './components/rfq-add/rfq-add.component';
import { HomeComponent } from './components/home/home.component';
import { NotAuthorizedComponent } from './components/not-authorized/not-authorized.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/authinterceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { NotificationsComponent } from 'src/app/components/notifications/notifications.component';
import { BusinessIntimationComponent } from './components/business-intimation/business-intimation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material';
import { CreateBusinessComponent } from './components/business-intimation/create-business/create-business.component';
import { UploadFilesComponent } from './components/upload-files/upload-files.component';
import { ModalModule } from 'ngx-bootstrap';
import { FileDropModule } from 'ngx-file-drop';
import { ClientsComponent } from './components/clients/clients.component';
import { CreateClientComponent } from './components/clients/create-client/create-client.component';
import { ClientDetailsComponent } from './components/clients/client-details/client-details.component';
import { CreateQuoteComponent } from './components/clients/create-quote/create-quote.component';
import { GeneralProposalComponent } from './components/clients/create-quote/general-proposal/general-proposal.component';
import { CoinsuranceComponent } from './components/clients/create-quote/coinsurance/coinsurance.component';
import { RiskDetailsComponent } from './components/clients/create-quote/risk-details/risk-details.component';
import { PastPolicyComponent } from './components/clients/create-quote/past-policy/past-policy.component';
import { PremiumComponent } from './components/clients/create-quote/premium/premium.component';
import { PopupComponent } from './components/popup/popup.component';
import { CreateHcnComponent } from './components/clients/create-hcn/create-hcn.component';
import { MembersDataComponent } from './components/members-data/members-data.component';
import { ManagePremiumComponent } from './components/manage-premium/manage-premium.component';
import { MembersMapHeadersComponent } from './components/members-data/members-map-headers/members-map-headers.component';
import { RaterComponent } from './components/rater/rater.component';
import { PolicyScheduleComponent } from './components/policy-schedule/policy-schedule.component';
import { ECardComponent } from './components/e-card/e-card.component';
@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    RfqActionComponent,
    RfqAddComponent,
    HomeComponent,
    NotAuthorizedComponent,
    NotificationsComponent,
    BusinessIntimationComponent,
    CreateBusinessComponent,
    UploadFilesComponent,
    ClientsComponent,
    CreateClientComponent,
    ClientDetailsComponent,
    CreateQuoteComponent,
    GeneralProposalComponent,
    CoinsuranceComponent,
    RiskDetailsComponent,
    PastPolicyComponent,
    PremiumComponent,
    PopupComponent,
    CreateHcnComponent,
    MembersDataComponent,
    ManagePremiumComponent,
    MembersMapHeadersComponent,
    RaterComponent,
    PolicyScheduleComponent,
    ECardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    MatTabsModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    FileDropModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
